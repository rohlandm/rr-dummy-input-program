# Compiler
CC = clang

# flags
CFLAGS = -Wall -g

SRC = src/main.c
TARGET = rr-dummy-input-program

all: $(TARGET)

$(TARGET): $(SRC)
	$(CC) $(CFLAGS) -o $(TARGET) $(SRC)

clean:
	rm rr-dummy-input-program
